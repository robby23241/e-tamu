<!DOCTYPE html>
<html>
<title>E-Tamu Diskominfo</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<link href="https://fonts.googleapis.com/css?family=Gudea&display=swap" rel="stylesheet">
<!-- Style -->
<link href="{{APP_ASSETS}}css/style.css" rel="stylesheet">
<link href="{{APP_ASSETS}}css/animate.css" rel="stylesheet">
<!-- Script  -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/locale/id.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment-with-locales.min.js"></script>

<script src="{{APP_ASSETS}}/js/webcam.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


<script src="https://cdn.rawgit.com/mckamey/countdownjs/master/countdown.min.js" type="text/javascript"></script>


<body>
  <div class="container-fluid bg-3 h-100">
    <div class="col-12 h-100">
      <div class="row">
        <!-- header  -->
        <div class="col-12">
          <div class="row">
            <div class="col-6 d-flex justify-content-center">
              <img class="img-logo" src="{{APP_ASSETS}}images/logo/diskominfo_bintan_logo.png" alt="logo kab bintan">
            </div>
            <div class="col-6 d-flex justify-content-center align-items-center">
              <h1>DIGITAL GUEST BOOK</h1>
            </div>
          </div>
        </div>
        <!-- header end  -->
        <!-- Form  -->
        <div class="col-12 col-md-6 col-lg-6 img-form p-3">
          <form id="register" action="">
            <div class="form-group">
              <label for="nama"><b>Nama Lengkap :</b></label>
              <input type="text" class="form-control" id="nama_lengkap" required>
            </div>
            <div class="form-group">
              <label for="pwd"><b>Instansi :</b></label>
              <select class="form-control" id="id_instansi" required>
                <option value="1">Diskominfo</option>
              </select>
            </div>
            <div class="form-group">
              <label for="pwd"><b>Jabatan :</b></label>
              <select class="form-control" id="id_jabatan" required>
                <option value="1">Staff</option>
              </select>
            </div>
            <div class="form-group">
              <label for="pwd"><b>Tujuan :</b></label>
              <textarea class="form-control" required cols="5"></textarea>
            </div>
            <button type="submit" class="btn btn-default">Submit</button>
          </form>
        </div>
        <!-- Camera Capture -->
        <div class="col-12 col-md-6">
          <div class="card img-clock">
            <h4 class=".countdown text-center" id="hari"></h4>
            <h4 class=".countdown text-center" id="tanggal"></h4>
          </div>
          <div id="camera">Capture</div>
          <div id="webcam">
            <input type=button value="Capture" onClick="preview()">
          </div>
          <div id="simpan" style="display:none">
            <input type=button value="Remove" onClick="batal()">
            <input type=button value="Save" onClick="simpan()" style="font-weight:bold;">
          </div>
          <div id="hasil"></div>
        </div>
      </div>
    </div>
    <div class="row bg-1">
      <div class="col-2">
        <img class="" src="{{APP_ASSETS}}images/female.png" alt="logo kab bintan">
      </div>
      <div class="col-8">
        <marquee direction="" scrollamount="8">
          <p class="text-white txt-lg funt pt-2"><b>SELAMAT DATANG DI DINAS KOMUNIKASI DAN INFORMATIKA KABUPATEN BINTAN</b></p>
        </marquee>
      </div>
    </div>
  </div>


  <script>
    // Open and close the sidebar on medium and small screens
    function w3_open() {
      document.getElementById("mySidebar").style.display = "block";
      document.getElementById("myOverlay").style.display = "block";
    }

    function w3_close() {
      document.getElementById("mySidebar").style.display = "none";
      document.getElementById("myOverlay").style.display = "none";
    }

    // Change style of top container on scroll
    window.onscroll = function() {
      myFunction()
    };

    function myFunction() {
      if (document.body.scrollTop > 80 || document.documentElement.scrollTop > 80) {
        document.getElementById("myTop").classList.add("w3-card-4", "w3-animate-opacity");
        document.getElementById("myIntro").classList.add("w3-show-inline-block");
      } else {
        document.getElementById("myIntro").classList.remove("w3-show-inline-block");
        document.getElementById("myTop").classList.remove("w3-card-4", "w3-animate-opacity");
      }
    }

    // Accordions
    function myAccordion(id) {
      var x = document.getElementById(id);
      if (x.className.indexOf("w3-show") == -1) {
        x.className += " w3-show";
        x.previousElementSibling.className += " w3-theme";
      } else {
        x.className = x.className.replace("w3-show", "");
        x.previousElementSibling.className =
          x.previousElementSibling.className.replace(" w3-theme", "");
      }
    }
  </script>

  <!-- Webcam Script -->
  <script language="Javascript">
    Webcam.set({
      width: 480,
      height: 300,
      image_format: 'jpeg',
      jpeg_quality: 100
    });
    Webcam.attach('#webcam');
  </script>
  <!-- Code to handle taking the snapshot and displaying it locally -->
  <script type="text/javascript">
    $('#register').on('submit', function(event) {
      event.preventDefault();
      var image = '';
      var nama_lengkap = $('#nama_lengkap').val();
      var id_instansi = $('#id_instansi').val();
      var id_jabatan = $('#id_jabatan').val();
      Webcam.snap(function(data_uri) {
        image = data_uri;
      });
      $.ajax({
          url: '<?php echo site_url("tamu/tambah"); ?>',
          type: 'POST',
          dataType: 'json',
          data: {
            nama_lengkap: nama_lengkap,
            id_instansi: id_instansi,
            id_jabatan: id_jabatan,
            image: image
          },
        })
        .done(function(data) {
          if (data > 0) {
            alert('Terima Kasih Telah Mengisi Buku Tamu');
            $('#register')[0].reset();
          }
        })
        .fail(function() {
          console.log("error");
        })
        .always(function() {
          console.log("complete");
        });


    });
  </script>

  <!-- Clock -->
  <!-- 
<script>
$(document).ready(function() {
setInterval( function() {
var hours = new Date().getHours();
$(".hours").html(( hours < 10 ? "0" : "" ) + hours);
}, 1000);
setInterval( function() {
var minutes = new Date().getMinutes();
$(".min").html(( minutes < 10 ? "0" : "" ) + minutes);
},1000);
setInterval( function() {
var seconds = new Date().getSeconds();
$(".sec").html(( seconds < 10 ? "0" : "" ) + seconds);
},1000);
});
</script> -->

  <!-- Moment Js -->

  <script>
    function displayTime() {
      // moment.locale('id');
      var tgl = moment().format('HH:mm:ss');
      var hari = moment().format('dddd');
      $('#tanggal').html(tgl);
      $('#hari').html(hari);
      setTimeout(displayTime, 1000);
    }

    $(document).ready(function() {
      displayTime();
    });
  </script>

</body>

</html>