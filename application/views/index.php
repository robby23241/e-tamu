<!DOCTYPE html>
<html>
<title>E-Tamu Diskominfo</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<link href="https://fonts.googleapis.com/css?family=Gudea&display=swap" rel="stylesheet">
<!-- Style -->
<link href="{{APP_ASSETS}}css/style.css" rel="stylesheet">
<link href="{{APP_ASSETS}}css/animate.css" rel="stylesheet">
<!-- Script  -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/locale/id.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment-with-locales.min.js"></script>

<script src="{{APP_ASSETS}}/js/webcam.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/locale/id.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<link rel="stylesheet" href="{{APP_ASSETS}}css/sweetalert2.css">
<link rel="stylesheet" href="{{APP_ASSETS}}css/sweetalert2.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


<script src="https://cdn.rawgit.com/mckamey/countdownjs/master/countdown.min.js" type="text/javascript"></script>

<script src="{{APP_ASSETS}}js/sweetalert2.js" type="text/javascript"></script>
<script src="{{APP_ASSETS}}js/sweetalert2.min.js" type="text/javascript"></script>
<script src="{{APP_ASSETS}}js/sweetalert2.all.js" type="text/javascript"></script>
<script src="{{APP_ASSETS}}js/sweetalert2.all.min.js" type="text/javascript"></script>


<body class="img-bg">
  <div class="container-fluid fullpage p-3">
      <div class="row">
        <!-- header  -->
        <div class="col-12">
          <div class="row">
            <div class="col-12 col-lg-6 d-flex justify-content-center">
              <img class="img-logo" src="{{APP_ASSETS}}images/logo/diskominfo_bintan_logo.png" alt="logo kab bintan">
            </div>
            <div class="col-12 col-lg-6 d-flex justify-content-center align-items-center">
              <img class="img-x-long" src="{{APP_ASSETS}}images/logo/e-tamu_logo.png" alt="logo Gebok">
            </div>
          </div>
        </div>

        <input type="hidden" id="date-part-val">
        <input type="hidden" id="time-part-val">
        <div class="col-12 col-md-6 col-lg-6 pt-3">
          <form id="register" class="img-form p-form">
            <div class="form-group col-12 col-lg-8 offset-lg-2">
              <div class="form-group">
                <label for="nama"><b>Nama Lengkap :</b></label>
                <input type="text" class="form-control form-control-sm" id="nama_lengkap" required>
              </div>
              <div class="form-group">
                <label for="email"><b>Email :</b></label>
                <input type="email" class="form-control form-control-sm" id="email" required>
              </div>
              <div class="form-group">
                <label for="instansi"><b>Instansi / Lembaga</b></label>
                <input type="text" class="form-control form-control-sm" id="id_instansi" required>
              </div>
              <!-- <div class="form-group">
                <label for="jabatan"><b>Jabatan</b></label>
                <input type="text" class="form-control form-control-sm" id="jabatan" required>
              </div> -->
              <div class="form-group">
                <label for="tujuan"><b>Tujuan :</b></label>
                <textarea class="form-control form-control-sm" required cols="5" id="tujuan"></textarea>
              </div>

            </div>
        </div>
        <!-- Camera Capture IDK IF WORKING -->
        <div class="col-12 col-md-6">
          <div class="text-center img-clock p-4">
            <h6 class=".countdown txt-b" id="hari"></h6>
            <h4 class=".countdown txt-b" id="tanggal"></h4>
          </div>
          <!-- <div id="camera">Capture</div> -->
          <div id="webcam" class="mx-auto">
           
          </div>
         
          

          <!-- <div id="simpan">
            <input type=button value="Remove" onClick="batal()">
            <input type=button value="Save" onClick="simpan()" style="font-weight:bold;">
          </div> -->
          <div id="hasil"></div>
          <div class="col-12 text-center pt-3" id="temp_webcam">
            <a href="javascript:void(0)" id="take"> <img class="img-submit index-2" src="{{APP_ASSETS}}images/capture_btn.png" alt="Capture Button"></a>
          </div>
          <div class="col-12 text-center pt-3" id="after_webcam" style="display: none;">
            <a href="javascript:void(0)" id="remove"> <img class="img-submit index-2" src="{{APP_ASSETS}}images/capture_cx.png" alt="Capture Button"></a>
          </div>
        </div>
      </div>
    
      <div class="row align-items-end pt-4 btm-lock">
        <!-- <div class="col-12 col-md-3 col-lg-2">
          <img class="img-custom-1 mx-auto" src="{{APP_ASSETS}}images/bujang_dara.png" alt="Bujang Dara">
        </div>
        <div class="col-12 col-md-9 col-lg-10"> -->
          <!-- <div class="row"> -->
          <!-- <div class="col-12 pb-4 bg-1"> -->
              <!-- <button type="submit" class="btn text-left"><img class="img-submit" src="{{APP_ASSETS}}images/submit_btn.png" alt="submit btn"></button> -->
              <!-- <a class="" href="#simpan"><img class="img-logo" src="{{APP_ASSETS}}images/submit_btn.png" alt="submit btn"></a> -->
            <!-- </div> -->
          <!-- </form>
          <marquee class="col-12 bg-1" direction="left" scrollamount="12">
              <p class="text-white txt-lg"><b>SELAMAT DATANG DI DINAS KOMUNIKASI DAN INFORMATIKA KABUPATEN BINTAN</b></p>
            </marquee>
        </div> -->
        <div class="col-12">
          <div class="row">
            <div class="col-12 pl-1">              
              <img class="img-custom-1 mx-auto index-1 pl-4 pb-2" src="{{APP_ASSETS}}images/bujang_dara.png" alt="Bujang Dara"> <!-- Gambar -->
              <button type="submit" class="btn text-left offset-lg-2 pb-4"><img class="img-submit" src="{{APP_ASSETS}}images/submit_btn.png" alt="submit btn"></button>
              </form>
            </div>
            <div class="col-12 bg-blue pt-1">
            <marquee class="col-11 offset-lg-2" direction="left" scrollamount="12">
              <p class="text-white txt-lg pt-1 pb-1 m-0"><b>SELAMAT DATANG DI DINAS KOMUNIKASI DAN INFORMATIKA KABUPATEN BINTAN</b></p>
            </marquee>
            </div>
          </div>
        </div>
      </div>
  </div>




  <script>
    // Open and close the sidebar on medium and small screens
    function w3_open() {
      document.getElementById("mySidebar").style.display = "block";
      document.getElementById("myOverlay").style.display = "block";
    }

    function w3_close() {
      document.getElementById("mySidebar").style.display = "none";
      document.getElementById("myOverlay").style.display = "none";
    }

    // Change style of top container on scroll
    window.onscroll = function() {
      myFunction()
    };

    function myFunction() {
      if (document.body.scrollTop > 80 || document.documentElement.scrollTop > 80) {
        document.getElementById("myTop").classList.add("w3-card-4", "w3-animate-opacity");
        document.getElementById("myIntro").classList.add("w3-show-inline-block");
      } else {
        document.getElementById("myIntro").classList.remove("w3-show-inline-block");
        document.getElementById("myTop").classList.remove("w3-card-4", "w3-animate-opacity");
      }
    }

    // Accordions
    function myAccordion(id) {
      var x = document.getElementById(id);
      if (x.className.indexOf("w3-show") == -1) {
        x.className += " w3-show";
        x.previousElementSibling.className += " w3-theme";
      } else {
        x.className = x.className.replace("w3-show", "");
        x.previousElementSibling.className =
          x.previousElementSibling.className.replace(" w3-theme", "");
      }
    }
  </script>

  <!-- Webcam Script -->
  <script language="Javascript">
      Webcam.set({
        width: 440,
        height: 360,
        dest_width: 640,
        dest_height: 480,
        image_format: 'jpeg',
        jpeg_quality: 100
      });
      Webcam.attach('#webcam');
    $('#take').click(function(){

      Webcam.freeze();
      document.getElementById('temp_webcam').style.display = 'none';
      document.getElementById('after_webcam').style.display = '';
      
    });

    $('#remove').click(function(){
        Webcam.unfreeze();
        document.getElementById('temp_webcam').style.display = '';
        document.getElementById('after_webcam').style.display = 'none';
    });
    
  </script>
  <!-- Code to handle taking the snapshot and displaying it locally -->
  <script type="text/javascript">

    let timerInterval


    $('#register').on('submit', function(event) {
      event.preventDefault();
      var image = '';
      var nama_lengkap = $('#nama_lengkap').val();
      var id_instansi = $('#id_instansi').val();
      var email = $('#email').val();
      var tujuan = $('#tujuan').val();
      var tgl_waktu = $('#date-part-val').val()+" "+$("#time-part-val").val();
      Webcam.snap(function(data_uri) {
        image = data_uri;
      });
      $.ajax({
          url: '<?php echo site_url("tamu/tambah"); ?>',
          type: 'POST',
          dataType: 'json',
          data: {
            nama_lengkap: nama_lengkap,
            id_instansi: id_instansi,
            image: image,
            email:email,
            tujuan:tujuan,
            tgl_waktu:tgl_waktu
          },
        })
        .done(function(data) {
          if (data > 0) {
            Swal.fire({
              title: 'Auto close',
              html: 'Akan tersimpan dalam <b></b> milliseconds.',
              timer: 2000,
              timerProgressBar: true,
              onBeforeOpen: () => {
                Swal.showLoading()
                timerInterval = setInterval(() => {
                  Swal.getContent().querySelector('b')
                    .textContent = Swal.getTimerLeft()
                }, 100)
              },
              onClose: () => {
                clearInterval(timerInterval)
              }
            }).then((result) => {
              if (
                /* Read more about handling dismissals below */
                result.dismiss === Swal.DismissReason.timer
              ) {
                console.log('I was closed by the timer') // eslint-disable-line
                $('#register')[0].reset();
                location.reload();
              }
            })

          }
        })
        .fail(function() {
          console.log("error");
        })
        .always(function() {
          console.log("complete");
        });


    });
  </script>
  <!-- Moment Js HELP!-->

  <script>
    function displayTime() {
      // moment.locale('id');
      var tgl = moment().format('HH:mm:ss');
      var hari = moment().format('dddd');
      $('#tanggal').html(tgl);
      $('#hari').html(hari);
      setTimeout(displayTime, 1000);
    }

    $(document).ready(function() {
      displayTime();
    });
  </script>


    <script type="text/javascript">
       $(document).ready(function() {
    var interval = setInterval(function() {
        moment.locale("id");
        var momentNow = moment();

        $('#date-part').html(momentNow.format('YYYY MMMM DD') + ' '
                            + momentNow.format('dddd'));
        $('#date-part-val').val(momentNow.format('YYYY-MM-DD'));


        $('#time-part').html(momentNow.format('HH:mm:ss'));
        $('#time-part-val').val(momentNow.format('HH:mm:ss'));
    }, 100);
    
    $('#stop-interval').on('click', function() {
        clearInterval(interval);
    });
});
    </script>

</body>

</html>