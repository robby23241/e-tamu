@extends("base.main")
@section("content")

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Rekap Data</h4>
                <p>Waktu Rekap Data</p>
                <form>
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <span class="form-control-plaintext">Mulai Tanggal</span>
                            <input type="date" class="form-control" placeholder="Mulai Tanggal" required>
                        </div>
                        <div class="col-12 col-md-6">
                            <span class="form-control-plaintext">Hingga Tanggal</span>
                            <input type="date" class="form-control" placeholder="Mulai Tanggal" required>
                        </div>
                    </div>
                    <button class="btn btn-primary mt-1" type="submit">Export (.xls)</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection