@extends("base.main")
@section("content")
    
     <div class="content-header row">
      </div>
      <div class="content-body">
       
        <!--/ fitness target -->
        <!-- activity charts -->
        <div class="row match-height">
          <div class="col-xl-8 col-lg-12">
            <div class="card">
              <div class="card-header border-0-bottom">
                <h4 class="card-title">Jumlah Pengunjung Mingguan
                </h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                  <ul class="list-inline mb-0">
                    <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                  </ul>
                </div>
              </div>
              <div class="card-content">
                <div class="card-body">
                  <div id="weekly-activity-chart" class="height-250"></div>
                  <ul class="list-inline text-center m-0">
                    <li>
                      <h6><i class="ft-circle danger"></i> Runnig</h6>
                    </li>
                    <li class="ml-1">
                      <h6><i class="ft-circle success"></i> Walking</h6>
                    </li>
                    <li class="ml-1">
                      <h6><i class="ft-circle warning"></i> Cycling</h6>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="col-xl-4 col-lg-12">
            <div class="card">
              <div class="card-content">
                <div class="card-body">
                  <div id="activity-division" class="height-250 echart-container"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!--/ activity charts -->
        <!--/ fitness info & twitter, facebook -->
      </div>
    </div>
  </div>
    
@endsection