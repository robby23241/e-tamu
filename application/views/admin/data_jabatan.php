@extends("base.main")


@section("content")
	<div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor"></h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <a href="{{site_url()}}opd/form_tambah" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i>Tambah Data</a>
                        </div>
                    </div>
                </div>

	 <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Data Jabatan</h4>
                                <h6 class="card-subtitle">Bisa untuk Export data ke CSV, Excel, PDF & Print</h6>
                                <div class="table-responsive m-t-40">
                                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>ID Jabatan</th>
                                                <th>Nama Jabatan</th>
                                                <th>Opsi</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>ID Jabatan</th>
                                                <th>Nama Jabatan</th>
                                                <th>Opsi</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                            @foreach($data->result() as $row)
                                            <tr style="font-weight: 700">
                                                <td>{{$row->id}}</td>
                                                <td>{{$row->nama_jabatan}}</td>
                                                <td><a href="{{site_url()}}opd/delete/{{$row->id}}" class="btn btn-info">Hapus</a></td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                        
@endsection
@section("js")
 <script>
    $(document).ready(function() {
        $('#myTable').DataTable();
        $(document).ready(function() {
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                "drawCallback": function(settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function(group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function() {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
        });
    });
    $('#example23').DataTable({
        dom: 'Bfrtip',
        buttons: [
            { extend: 'pdf', className: 'btn-primary' },
            { extend: 'print', className: 'btn-primary' },
            { extend: 'excel', className: 'btn-primary' }
        ]
    });
    </script>
    @endsection