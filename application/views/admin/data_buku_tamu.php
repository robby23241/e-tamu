@extends("base.main")

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.0/css/buttons.dataTables.min.css">
@section("content")
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor"></h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <!-- <a href="{{site_url()}}opd/form_tambah" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i>Tambah Data</a> -->
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Data buku tamu</h4><!-- 
                <h6 class="card-subtitle">Bisa untuk Export data ke CSV, Excel, PDF & Print</h6> -->
                <div class="table-responsive m-t-40">
                    <table id="example23" class="table table-striped table-bordered complex-headers" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Nama Lengkap</th>
                                <th>Instansi</th>
                                <th>Gambar</th>
                                <th>Tujuan</th>
                                <th>Tanggal / Waktu</th>
                                <th>Opsi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data->result() as $row)
                            <tr style="font-weight: 700">
                                <td>{{$row->nama_lengkap}}</td>
                                <td>{{$row->id_instansi}}</td>
                                <td><img src="{{ASSETS_UPLOAD}}{{$row->image}}" style="width: 100%; height: 50%;"></td>
                                <td>{{$row->tujuan}}</td>
                                <td>{{$row->tanggal_waktu}}</td>
                                <td><a href="{{site_url()}}tamu/delete/{{$row->id}}" class="btn btn-info">Hapus</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

                        
@endsection
@section("js")
 

 <script>
      
    </script>
    @endsection