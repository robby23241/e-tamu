@php
  defined('BASEPATH') OR exit('No direct script access allowed');
@endphp
<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Stack admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
  <meta name="keywords" content="admin template, stack admin template, dashboard template, flat admin template, responsive admin template, web app">
  <meta name="author" content="PIXINVENT">
  <title>{{APP_NAME}} - Admin</title>
  <link rel="apple-touch-icon" href="{{APP_ASSETS}}images/ico/apple-icon-120.png">
  <link rel="shortcut icon" type="image/x-icon" href="{{APP_ASSETS}}images/ico/favicon.ico">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i"
  rel="stylesheet">
  <!-- BEGIN VENDOR CSS-->
  <link rel="stylesheet" type="text/css" href="{{APP_ASSETS}}css/vendors.css">
  <link rel="stylesheet" type="text/css" href="{{APP_ASSETS}}vendors/css/forms/icheck/icheck.css">
  <link rel="stylesheet" type="text/css" href="{{APP_ASSETS}}vendors/css/forms/icheck/custom.css">
  <link rel="stylesheet" type="text/css" href="{{APP_ASSETS}}vendors/css/charts/morris.css">
  <link rel="stylesheet" type="text/css" href="{{APP_ASSETS}}vendors/css/extensions/unslider.css">
  <link rel="stylesheet" type="text/css" href="{{APP_ASSETS}}vendors/css/weather-icons/climacons.min.css">
  <!-- END VENDOR CSS-->
  <!-- BEGIN STACK CSS-->
  <link rel="stylesheet" type="text/css" href="{{APP_ASSETS}}css/app.css">
  <!-- END STACK CSS-->
  <!-- BEGIN Page Level CSS-->
  <link rel="stylesheet" type="text/css" href="{{APP_ASSETS}}css/core/menu/menu-types/vertical-menu.css">
  <link rel="stylesheet" type="text/css" href="{{APP_ASSETS}}css/core/colors/palette-climacon.css">
  <link rel="stylesheet" type="text/css" href="{{APP_ASSETS}}css/core/colors/palette-gradient.css">
  <link rel="stylesheet" type="text/css" href="{{APP_ASSETS}}fonts/simple-line-icons/style.min.css">
  <link rel="stylesheet" type="text/css" href="{{APP_ASSETS}}fonts/meteocons/style.min.css">
  <link rel="stylesheet" type="text/css" href="{{APP_ASSETS}}css/pages/users.css">
  <!-- END Page Level CSS-->
  <!-- BEGIN Custom CSS-->
  <link href="{{APP_ASSETS}}css/style.css" rel="stylesheet">
  <!-- END Custom CSS-->
  @show
</head>
 <body class="vertical-layout vertical-menu 2-columns menu-expanded fixed-navbar"data-open="click" data-menu="vertical-menu" data-col="2-columns">
    <nav class="header-navbar navbar-expand-md navbar navbar-with-menu fixed-top navbar-light navbar-border">
    <div class="navbar-wrapper">
      <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
          <li class="nav-item mobile-menu d-md-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu font-large-1"></i></a></li>
          <li class="nav-item">
            <a class="navbar-brand" href="{{site_url()}}admin">
              <h2 class="brand-text"><img class="img-brand" alt="stack admin logo" src="{{APP_ASSETS}}images/logo/e-tamu_logo.png"></h2>
            </a>
          </li>
          <li class="nav-item d-md-none">
            <a class="nav-link open-navbar-container" data-toggle="collapse" data-target="#navbar-mobile"><i class="fa fa-ellipsis-v"></i></a>
          </li>
        </ul>
      </div>
      <div class="navbar-container content">
        <div class="collapse navbar-collapse" id="navbar-mobile">
          <ul class="nav navbar-nav mr-auto float-left">
            <li class="nav-item d-none d-md-block"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu"></i></a></li>
            
            
          </ul>
          <ul class="nav navbar-nav float-right">
            
            <li class="dropdown dropdown-user nav-item">
              <a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
                <span class="avatar avatar-online">
                  <img src="{{APP_ASSETS}}images/bujang_dara.png" alt="avatar"><i></i></span>
                <span class="user-name">Admin</span>
              </a>
              <div class="dropdown-menu dropdown-menu-right">
                <a class="dropdown-item" href="{{site_url()}}login/logout"><i class="ft-power"></i> Logout</a>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </nav>
  <div class="main-menu menu-fixed menu-light menu-accordion" data-scroll-to-active="true">
    <div class="main-menu-content">
      <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
        <li class=" navigation-header">
          <span>Main Menu</span><i class=" ft-minus" data-toggle="tooltip" data-placement="right"data-original-title="General"></i>
        </li>
        <li class=" nav-item">
        <a href="{{site_url()}}admin"><i class="ft-home"></i><span class="menu-title" data-i18n="">Dashboard</span></a>
        </li>
        <li class=" nav-item">
          <a href="{{site_url()}}tamu"><i class="ft-monitor"></i><span class="menu-title" data-i18n="">Daftar Tamu</span></a>
        </li>
        <li class=" nav-item">
          <a href="{{site_url()}}rekap"><i class="ft-file-text"></i><span class="menu-title" data-i18n="">Rekap Data</span></a>
        </li>
            
      </ul>
    </div>
  </div>
  <div class="app-content content">
    <div class="content-wrapper">
    @yield('content')
        </div>
    </div>
      <footer class="footer footer-static footer-light navbar-border">
    <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2">
      <span class="float-md-left d-block d-md-inline-block">Copyright &copy; 2020 Diskominfo Bintan All rights reserved</span>
    </p>
  </footer>
  <!-- BEGIN VENDOR JS-->
  <script src="{{APP_ASSETS}}vendors/js/vendors.min.js" type="text/javascript"></script>
  <!-- BEGIN VENDOR JS-->
  <script src="{{APP_ASSETS}}vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
 
  <script src="{{APP_ASSETS}}js/scripts/tables/datatables/datatable-basic.js"
  type="text/javascript"></script>
  <!-- BEGIN PAGE VENDOR JS-->
  <script src="{{APP_ASSETS}}vendors/js/charts/gmaps.min.js" type="text/javascript"></script>
  <script src="{{APP_ASSETS}}vendors/js/forms/icheck/icheck.min.js" type="text/javascript"></script>
  <script src="{{APP_ASSETS}}vendors/js/extensions/jquery.knob.min.js" type="text/javascript"></script>
  <script src="{{APP_ASSETS}}vendors/js/charts/raphael-min.js" type="text/javascript"></script>
  <script src="{{APP_ASSETS}}vendors/js/charts/morris.min.js" type="text/javascript"></script>
  <script src="{{APP_ASSETS}}vendors/js/charts/jquery.sparkline.min.js" type="text/javascript"></script>
  <script src="{{APP_ASSETS}}vendors/js/extensions/unslider-min.js" type="text/javascript"></script>
  <script src="{{APP_ASSETS}}vendors/js/charts/echarts/echarts.js" type="text/javascript"></script>
  <!-- END PAGE VENDOR JS-->
  <!-- BEGIN STACK JS-->
  <script type="text/javascript" src="{{APP_ASSETS}}vendors/js/charts/echarts/chart/pie.js"></script>
  <script type="text/javascript" src="{{APP_ASSETS}}vendors/js/charts/echarts/chart/funnel.js"></script>
  <script src="{{APP_ASSETS}}js/core/app-menu.js" type="text/javascript"></script>
  <script src="{{APP_ASSETS}}js/core/app.js" type="text/javascript"></script>
  <script src="{{APP_ASSETS}}js/scripts/customizer.js" type="text/javascript"></script>
  <!-- END STACK JS-->

  <!-- BEGIN PAGE LEVEL JS-->
  <script src="{{APP_ASSETS}}js/scripts/pages/dashboard-fitness.js" type="text/javascript"></script>

   

    

  <!-- END PAGE LEVEL JS-->
  @yield("js")
</body>
</html>
