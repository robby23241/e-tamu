<?php

	class Instansi extends CI_Controller
	{
		public function __construct()
		{
			parent::__construct();
		}

		public function index()
		{
			$this->slice->with("data",$this->data());
			view("admin.data_instansi");
		}

		public function data()
		{
			$query = $this->db->query("select * from instansi");
			return $query;
		}
	}
?>