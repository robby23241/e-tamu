<?php
	
	class Jabatan extends CI_Controller
	{
		public function __construct()
		{
			parent::__construct();
		}

		public function index()
		{
			$this->slice->with("data",$this->data());
			view("admin.data_jabatan");
		}

		public function data()
		{
			$query = $this->db->query("select * from jabatan");
			return $query;
		}
	}

?>