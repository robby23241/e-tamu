<?php

	class Rekap extends CI_Controller
	{
		public function __construct()
		{
			parent::__construct();
			if($this->session->userdata("login")!=true)
			{
				redirect("login");
			}
		}

		public function index()
		{

			view("admin.data_rekap");

		}
		
	}

?>