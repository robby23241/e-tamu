<?php

	class Tamu extends CI_Controller
	{
		public function __construct()
		{
			parent::__construct();
		}

		public function index()
		{
			$this->slice->with("data",$this->data());
			view("admin.data_buku_tamu");
		}

		public function tambah()
		{
			$nama_lengkap = $this->input->post('nama_lengkap', true);
			$id_instansi = $this->input->post('id_instansi', true);
			$image = $this->input->post('image');
			$image = str_replace('data:image/jpeg;base64,','', $image);
			$image = base64_decode($image);
			$filename = 'image_'.time().'.png';
			file_put_contents(FCPATH.'/uploads/'.$filename,$image);
			$email = $this->input->post('email', true);
			$tujuan = $this->input->post('tujuan', true);
			$tgl_waktu = $this->input->post("tgl_waktu",true);
			$data = array(
				'nama_lengkap' => $nama_lengkap,
				'id_instansi' => $id_instansi,
				'image' => $filename,
				'email' => $email,
				'tujuan' => $tujuan,
				'tanggal_waktu'=>$tgl_waktu
			);
			
			$res = $this->db->insert("buku_tamu",$data);
			echo json_encode($res);
		}

		public function delete($id)
		{
			$query = $this->db->query("delete from buku_tamu where id='$id' ");
			if($query)
			{
				redirect("tamu");
			}
		}

		public function data()
		{
			$query = $this->db->query("select * from buku_tamu");
			return $query;
		}
	}
?>